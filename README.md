# **RTEMS6 Port of the GPU4S Bench - OBPMark-Kernel**
## **Authors**
- Ivan Rodriguez Ferrandez (UPC-BSC)
- Alvaro Jover-Alvarez (UPC-BSC)
- Leonidas Kosmidis (BSC-UPC)
- David Steenari (ESA)

## **RTEMS6 Port**
- Jannis Wolf (BSC)

### **Version: 1.0**  

<br/>

## **Description**
Embedded  GPUs  have  been  identified  from  both private  and  government  space  agencies  as  promising  hardware technologies to satisfy the increased needs of payload processing.The GPU4S (GPU for Space) project funded from the EuropeanSpace  Agency  (ESA)  has  explored  in  detail  the  feasibility  and the  benefit  of  using  them  for  space  workloads.  Currently  at  the closing phases of the project, in this paper we describe the main project outcomes and explain the lessons we learnt. In addition,we  provide  some  guidelines  for  the  next  steps  towards  their adoption  in  space.


## **Implemented Languages**
- ~~Standard C~~
- ~~CUDA~~
- ~~OpenCL~~
- OpenMP
- ~~HIP~~
  
**NOTE:** This repository contains a port to RTEMS6 leveraging the OpenMP implementations. All other implementations were removed. For single processor standard c just change the omp_max_thread variable in init.c to 1.

## **Benchmark List and Basic Description**

For most of the benchmark suite there is a naïve, optimize and library versions. The benchmarks with their implementations are listed below.
- [x] Cifar 10
  - Naïve,optimize ~~and library (only for CUDA)~~
- [x] Cifar 10 Multiple
  - Naïve,optimize ~~and library (only for CUDA)~~
- [x] Convolution 2D
  - Naïve,optimize ~~and library (only for CUDA)~~
- [x] Correlation 2D
  - Naïve,optimize
- [ ] ~~Fast fourier transform 2D bench~~ __gpu only benchmark__
  - Library
- [x] Fast fourier transform
  - Naïve,optimize ~~and library~~
- [x] Fast fourier transform Window
  - Naïve,optimize ~~and library~~
- [x] Finite impulse response filter
  - Naïve
- [x] Local response normalization (LRN)
  - Naïve,optimize ~~and library (only for CUDA)~~
- [x] Matrix multiplication
  - Naïve,optimize ~~and library~~
- [x] Max pooling bench
  - Naïve,optimize ~~and library (only for CUDA)~~
- [ ] ~~Memory Bandwidth~~ __gpu only benchmark__ 
  - Naïve
- [x] Relu
  - Naïve,optimize and ~~library (only for CUDA)~~
- [x] Softmax
  - Naïve,optimize and ~~library (only for CUDA)~~
- [x] Wavelet transform
  - Naïve,optimize

**NOTE:** At the moment only the naïve and the optimized version are ported.

## **Benchmark Compilation**
~~For compile each of the benchmarks first you need to go to the folder for the specific benchmark that you want to compile.
Inside of the folder you can call the Makefile for compilation. All of the Makefiles behaves the same for compilation.~~ 

~~There is three parts for the make file. 
First is the type of benchmark that you want to compile, that could be *cuda* (this will compile cuda naïve) will be the same for the rest of the languages, for different version will be will the suffixes -opt, -lib for the optimize and library versions, example cuda-opt, opencl-lib.~~

~~The second part is the definition of the data type, for all of the benchmarks float and double is supported and some of the benchmarks supports also integer. For specify the data type you need to add *-DATATYPE=(language)* for the languages the naming is in capital letters and are FLOAT,DOUBLE and INT.~~ 

~~The last parameter is the block size, this is only needed for the GPU code versions (OpenMP does not need this parameter). For the Makefile you need to provide *-BLOCKSIZE=(SIZE)* the block size is use square of the size that you provide, the recommended values are 4,8,16,32.~~

~~A full example will be as follows~~

~~``` make opencl-opt DATATYPE=FLOAT BLOCKSIZE=16 ```~~

~~The compiled binary will be in the bin folder.~~

While the original benchmark suite was using Makefiles, this port utilizes waf as build system, as this is the building tool that is supported from rtems. 

To compile the benchmark, a running rtems installation is needed. The version 6 of rtems was tested to work on riscv/noel64imafdc and qemu for riscv/rv64imafdc. It is recommended to use [this Dockerfile](https://gitlab.bsc.es/wjannis/rtems-docker), which creates an image with everything installed (RTEMS toolchain for RISC-V, BSPs for NOEL-V and RISC-V for QEMU, QEMU-SYSTEM-RISCV64).

Clone this repo from inside the newly created Docker container. Every subtest of the benchmark has its own wscript, which is the "Makefile" of waf.

Changes in the configuration of data type need to be done here in the wscript file. In line 17 there is the variable DATATYPE to set this value (float by default). In line 18 additional CXX compiler flags can be added:

```python
DATATYPE = 'FLOAT'
CPPFLAGS = '-D' + DATATYPE + ' -DOPENMP -g -O3 -fopenmp'
```

Then call either
```bash
./configure  # for NOEL-V
# or
./configure-sim-qemu  # for RISC-V QEMU
```
to configure for the respective BSP, followed by
```bash
./waf
```
to compile the binaries benchmark.exe (and benchmark-opt.exe if it is implemented). It is located afterwards in the build/rtems-riscv-*64imafdc folder.

To simulate with qemu, call
```bash
./sim-qemu  # for the naive implementation
# or
./sim-qemu-opt  # for the optimized version (if it exists)
```

__Benchmark specific parameters:__ Apart from the general building guide, every benchmark has specific parameters. Without RTEMS, the benchmarks could be called with commandline parameters. This is not possible at the moment. The flags need to be coded into the apptask.c function. In the beginning of the file apptask.c there are the two macros to initialize __argc__ and __argv__. Here all flags can be specified. After any changes, the benchmark needs to be recompiled obviously.

So for example for the matrix_multiplication_bench

```c
// {name of benchmark, flag for size of matrices (needs to be always set), value of size of matrices, print timings}
#define ARGV {"matrix_multiplication", "-s", "512", "-t"} 
// number of arguments
#define ARGC 4
```

## **Current limitations**

RTEMS does not support the getline() function, as it is not a function from the C standard library. Therefore it is not possible to use input files for matrix initialization. A solution using fgets() is under developement.

## **Changes performed for porting**
The port to RTEMS needed only minimal changes:

- Embedding of the main function which is the normal entry point of the benchmark into a rtems single application apptask. More infos about that can be found in [here](https://gitlab.bsc.es/wjannis/rtems-docker#port-applications-to-rtems)
- Port from make to waf by translating Makefile to wscript
- RTEMS does not support CLOCK_MONOTONIC_RAW. Change to CLOCK_MONOTONIC in every main.cpp by
```bash
sed -i 's/CLOCK_MONOTONIC_RAW/CLOCK_MONOTONIC/g' main.cpp
```
- Like mentioned in the current limitations section, declaration of an empty getline() function in cpu_functions.h to avoid linking error while building
