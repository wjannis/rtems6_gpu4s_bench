#include "system.h"
//#include "tmacros.h"

#include <stdio.h>
#include <stdlib.h>

#include <rtems.h>
#include <rtems/bspIo.h>
#include <rtems/libcsupport.h>
#include <stdint.h>

#include "main.h"
#include "parameter.h"


rtems_task Application_task(
  rtems_task_argument argument
)
{
  rtems_id          tid;
  rtems_status_code status;
  unsigned int      a = (unsigned int) argument;

  status = rtems_task_ident( RTEMS_WHO_AM_I, RTEMS_SEARCH_ALL_NODES, &tid );
  char* argv[] = ARGV_RELU;
  int argc = ARGC_RELU;
  printf("%s\n", argv[0]);
  int i = 0;
  while(i<=10){
    main(argc, argv);
    i++;
  }
  
  exit( 0 );
}
