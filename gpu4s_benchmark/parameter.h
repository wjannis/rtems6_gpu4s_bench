#define OMP_NUM_THREADS_MACRO "1"

#define ARGV_LRN {"lrn", "-s", "512", "-t"}
#define ARGC_LRN 4;

#define ARGV_CIFAR {"cifar_10", "-t"}
#define ARGC_CIFAR 2

#define ARGV_CIFAR_MULTIPLE {"cifar_multiple", "-s", "512", "-t"}
#define ARGC_CIFAR_MULTIPLE 4

#define ARGV_CONVOLUTION_2D {"convolution_2d", "-s", "512", "-t", "-k", "3"}
#define ARGC_CONVOLUTION_2D 6

#define ARGV_CORRELATION_2D {"correlation_2d", "-s", "512", "-t"}
#define ARGC_CORRELATION_2D 4

#define ARGV_FAST_FOURIER_TRANSFORM {"fast_fourier_transform", "-s", "512", "-t"}
#define ARGC_FAST_FOURIER_TRANSFORM 4

#define ARGV_FAST_FOURIER_TRANSFORM_WINDOW {"fast_fourier_transform_window", "-s", "512", "-t"}
#define ARGC_FAST_FOURIER_TRANSFORM_WINDOW 4

#define ARGV_FINITE_IMPULSE_RESPONSE {"finite_impulse_response", "-s", "512", "-t"}
#define ARGC_FINITE_IMPULSE_RESPONSE 4

#define ARGV_MATRIX_MULTIPLICATION {"matrix_multiplication", "-s", "256", "-t"}
#define ARGC_MATRIX_MULTIPLICATION 4

#define ARGV_MAX_POOLING {"max_pooling", "-s", "512", "-t", "-l", "2"}
#define ARGC_MAX_POOLING 6

#define ARGV_RELU {"relu", "-s", "512", "-t"}
#define ARGC_RELU 4

#define ARGV_SOFTMAX {"softmax", "-s", "512", "-t"}
#define ARGC_SOFTMAX 4

#define ARGV_WAVELET_TRANSFORM {"wavelet_transform", "-s", "512", "-t"}
#define ARGC_WAVELET_TRANSFORM 4
